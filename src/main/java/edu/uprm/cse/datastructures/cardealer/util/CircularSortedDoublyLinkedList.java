package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {

	//Iterator Class
	private class SCDLLIterator<E> implements Iterator<E> {
		//Instance var
		private Node<E> nextNode;

		//Contructors
		@SuppressWarnings("unchecked")
		public SCDLLIterator() {
			this.nextNode = (Node<E>) header.getNext();
		}
		@SuppressWarnings("unchecked")
		public SCDLLIterator(int index) {
			int count = 0;
			this.nextNode = (Node<E>) header;
			while(count <= index) {
				this.nextNode = this.nextNode.getNext();
				count++;
			}	
		}
		@Override
		public boolean hasNext() {
			return nextNode != header;
		}

		@Override
		public E next() {
			if (this.hasNext()) {
				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}
	}


	//Reverse iterator class implementing reverse iterator interface
	@SuppressWarnings("hiding")
	private class ReverseSCDLLIterator<E> implements ReverseIterator<E> {
		//instance Var
		private Node<E> prevNode;

		//Constructors
		@SuppressWarnings("unchecked")
		public  ReverseSCDLLIterator() {
			this.prevNode = (Node<E>) header.getPrev();	
		}

		@SuppressWarnings("unchecked")
		public  ReverseSCDLLIterator(int index) {			
			int count = 0;
			this.prevNode = (Node<E>) header;
			while(count <= index) {
				this.prevNode = this.prevNode.getPrev();
				count++;
			}
		}

		@Override
		public E previous() {
			if (this.hasPrevious()) {
				E result = this.prevNode.getElement();
				this.prevNode = this.prevNode.getPrev();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}
		@Override
		public boolean hasPrevious() {
			return prevNode.element != null;
		}
	}	

	//Node class
	private static class Node<E> {
		private E element;
		private Node<E> next;
		private Node<E> prev;

		public void disableConections() {
			this.next=null;
			this.prev=null;
			this.element=null;

		}
		public Node(E element, Node<E> next, Node<E> prev) {
			super();
			this.element = element;
			this.next = next;
			this.prev = prev;
		}

		public Node() {
			super();
		}

		public E getElement() {
			return element;
		}

		public void setElement(E element) {
			this.element = element;
		}

		public Node<E> getNext() {
			return next;
		}

		public void setNext(Node<E> next) {
			this.next = next;
		}

		public Node<E> getPrev() {
			return prev;
		}

		public void setPrev(Node<E> prev) {
			this.prev = prev;
		}
	}


	//Intance Variables
	@SuppressWarnings("rawtypes")
	private Comparator comp;
	private Node<E> header;
	private int currentSize;

	//Constructor
	@SuppressWarnings("rawtypes")
	public CircularSortedDoublyLinkedList(Comparator comp) {
		this.currentSize = 0;
		this.header = new Node<>();

		this.header.setElement(null);
		this.header.setNext(this.header);
		this.header.setPrev(this.header);
		if(comp!=null) {
			this.comp=comp;
		}
		else {
			this.comp= new CarComparator();
		}
	}

	//Adds new elements to the list, it will add it in the correct position, return true if successful
	@SuppressWarnings("unchecked")
	@Override
	public boolean add(E obj) {
		int currentPos = 0;
		Node<E> newNode = new Node<>();
		Node<E> temp = this.header;


		if (this.isEmpty()) {
			newNode = new Node<>(obj, this.header, this.header);
			this.header.setNext(newNode);
			this.header.setPrev(newNode);

		} 
		else {
			while (currentPos <= this.size()) {

				temp = temp.getNext();
				E tempObj = temp.getElement();
				currentPos++;

				if (this.comp.compare(tempObj, obj) >= 0) {
					newNode = new Node<>(obj, temp, temp.getPrev());
					temp.getPrev().setNext(newNode);
					temp.setPrev(newNode);
					break;
				} 
				else if (this.size() == currentPos) {
					newNode = new Node<>(obj, temp.getNext(), temp);
					this.header.getPrev().setNext(newNode);
					this.header.setPrev(newNode);
					break;
				}
			}
		}
		this.currentSize++;
		return true;
	}
	//Return the current size of the list
	@Override
	public int size() {
		return this.currentSize;
	}

	//remove the first instance of the object specified, return true if erasure was successful
	@Override
	public boolean remove(E obj) {
		Node<E> temp = this.header;
		int currentPos = 0;
		Node<E> target = null;


		if (this.isEmpty() || this.firstIndex(obj) < 0) {
			return false;
		}

		while (currentPos != this.firstIndex(obj)) {
			temp = temp.getNext();
			currentPos++;
		}

		target = temp.getNext();
		temp.setNext(target.getNext());
		target.getNext().setPrev(temp);
		target.disableConections();
		this.currentSize--;

		return true;
	}
	//Remove the object at specified index in the list, return true if successful
	@Override
	public boolean remove(int index) {
		return this.remove(this.get(index));
	}
	//Remove all instances of the objects in the list; returns the amount of object erased
	@Override
	public int removeAll(E obj) {
		int count = 0;
		while (this.remove(obj)){
			count++;
		}
		return count;
	}

	//Return the first element in the list, return null if empty
	@Override
	public E first() {
		if(this.isEmpty()) {
			return null;
		}
		else {
			return this.header.getNext().getElement();
		}

	}
	//Return last element in the list, return null if empty
	@Override
	public E last() {
		if(this.isEmpty()) {
			return null;
		}
		else {
			return this.header.getPrev().getElement();
		}	
	}
	//Return the element of specifies index
	@Override
	public E get(int index) {
		if (index < 0 || index > this.size()-1) throw new IndexOutOfBoundsException();

		if (this.isEmpty()) {
			return null;
		}

		Node<E> tmp = this.header;
		int currentPos = 0;

		while (currentPos != index) {
			tmp = tmp.getNext();
			currentPos++;
		}
		return tmp.getNext().getElement();
	}

	//ERASE THE ENTIRETY OF THE LSIT
	@Override
	public void clear() {
		while (!this.isEmpty()) {
			this.remove(0);
		}
	}
	//Check if list contains Specified element, return true if it exists otherwise false.
	@Override
	public boolean contains(E e) {
		return this.firstIndex(e) >= 0;
	}
	//Check if the list Empty
	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}
	//Return the first index where the specified element is located
	@Override
	public int firstIndex(E e) {

		int i = 0;
		for (Node<E> temp = this.header.getNext(); temp != this.header;
				temp = temp.getNext(), i++) {
			if (temp.getElement().equals(e)) {
				return i;
			}
		}
		return -1;
	}
	//Return the last index where the specified element is located
	@Override
	public int lastIndex(E e) {

		int i = this.currentSize-1;
		for (Node<E> temp = this.header.getPrev(); temp != this.header; temp = temp.getPrev(), i--) {
			if (temp.getElement().equals(e)){
				return i;
			}
		}
		return -1;

	}
	public Object[] toArray() {
		if (this.isEmpty()) {
			return new Object[0];
		}
		Object[] result = new Object[this.size()];

		Node<E> tmp = this.header;
		int currentPos = 0;

		while (currentPos < this.size()) {
			tmp = tmp.getNext();
			result[currentPos] = tmp.getElement();
			currentPos++;
		}
		return result;
	}
	//Iterator Constructors
	@Override
	public Iterator<E> iterator(int index) {		
		return new SCDLLIterator<E>(index);
	}
	@Override
	public Iterator<E> iterator() {		
		return new SCDLLIterator<E>();
	}
	@Override
	public ReverseIterator<E> reverseIterator() {		
		return new ReverseSCDLLIterator<E>();
	}
	@Override
	public ReverseIterator<E> reverseIterator(int index) {		
		return new ReverseSCDLLIterator<E>(index);
	}


}
