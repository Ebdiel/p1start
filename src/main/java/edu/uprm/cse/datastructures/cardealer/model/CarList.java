package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

public class CarList {
	private static final CircularSortedDoublyLinkedList<Car> carlist = new CircularSortedDoublyLinkedList<Car>(new CarComparator());
	
	//Carlist as a Global variable
	public static CircularSortedDoublyLinkedList<Car> getInstance(){
		return carlist;

	}
	
	//Clears all cars in the list
	public static void resetCars() {
		carlist.clear();		
	}
	
}
