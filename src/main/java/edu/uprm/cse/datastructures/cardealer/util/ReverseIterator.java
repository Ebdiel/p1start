package edu.uprm.cse.datastructures.cardealer.util;

public interface ReverseIterator<E> {
	//Reverse iterator interface
	public E previous();
	public boolean hasPrevious();
	
}
