package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car>{

	@Override
	public int compare (Car c1, Car c2) {
		//Comparator implements compareTo for each option
		int brand= c1.getCarBrand().compareTo(c2.getCarBrand());
		int model= c1.getCarModel().compareTo(c2.getCarModel());
		int modelOption= c1.getCarModelOption().compareTo(c2.getCarModelOption());
		double price= c1.getCarPrice() - c2.getCarPrice();
		
		
		if (brand != 0) {
			return brand;
		}
		else if (model != 0) 	{
			return model;
		}
		else if (modelOption != 0) {
			return modelOption;
		}
		else if (price != 0) {		
			return (int) price;
		}
	
		return 1;
		
	}
	

}
