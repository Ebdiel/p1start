package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

@Path("/cars")
public class CarManager {

	private CircularSortedDoublyLinkedList<Car> listOfCars = CarList.getInstance();
	
	//this gets all cars in the server
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		Car[] result = new Car[listOfCars.size()];
		if (listOfCars.isEmpty()) {
			return new Car[0];
		}
		for (int i = 0; i < this.listOfCars.size(); i++) {
			result[i] = listOfCars.get(i);
		}

		return result;
	}
	
	//Adds a car object to the server
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car){
//		for (int i = 0; i < listOfCars.size(); i++) {
//			if (listOfCars.get(i).getCarId() == car.getCarId()){
//				return Response.status(Response.Status.BAD_REQUEST).build();
//				cannot add same id car
//			}
//		}


		listOfCars.add(car);
		return Response.status(201).build();
	}
	
	//Deletes a car with the specified ID
	@DELETE
	@Path("/{id}/delete")
	public Response deleteCar(@PathParam("id") long id){
		for (int i = 0; i < this.listOfCars.size(); i++) {
			if (this.listOfCars.get(i).getCarId() == id) {
				this.listOfCars.remove(this.listOfCars.get(i));
				return Response.status(Response.Status.OK).build(); 
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();      

	}
	//Displays the car with specified ID
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id){

		for (int i = 0; i < this.listOfCars.size(); i++) {
			if (this.listOfCars.get(i).getCarId() == id) {
				return this.listOfCars.get(i);
			
			}
			
			
		}
		/** @Error404: Car not found */
		throw new WebApplicationException(404); 
		
		      
//
	}
	//Updates the car with the specified ID
	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(@PathParam("id") long id, Car car) {
		for (int i = 0; i < this.listOfCars.size(); i++) {
			if (this.listOfCars.get(i).getCarId() == id) {
				this.listOfCars.remove(this.listOfCars.get(i));
				this.listOfCars.add(car);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();      

	}

//		public Response NotFound() {
//		return Response.status(Response.Status.NOT_FOUND).build();      
//
//		
//	}
	
}

